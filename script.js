console.warn(`Hello World`);

// objects

function phone(){
	let phone = {
		brand: `Sony`,
		model: `Xperia pro-I`,
		color: `Black`,
		processor: `Snapdragon 888`
	};

	console.warn(phone);
	console.warn(typeof(phone));
}

// using constructor function -- like blueprint or template to create an object form
function laptop(brand, model, arr){
	this.mybrand = brand;
	this.mymodel = model;
	this.myarrr = arr;
}

let laps = new laptop(`Acer`, `Predator` [1,2,3,4,5,6]);
let laps2 = new laptop(`Sony`, `Vaio`, [2,4,46,65,65,6,2]);
let arr1 = [laps, laps2];

phone();
console.log(laps);
console.log(arr1);
console.log(arr1[0].mybrand);
console.log(arr1[1][`mybrand`]);
console.log(laps2[`mymodel`]);

laps2[`Year`] = `2000s`;
console.log(laps2);

//remove object property
delete laps2['Year'];
console.warn(laps2);

let user = {
	name: `Marvin`,
	email: [`123@email.com`, `456@gmail.com`]
};

console.warn(user.email);
console.warn(user.email[0]);

// object methods

let person = {
	name: `Marvin`,
	talk: function(){
		console.log(`I am ${this.name}`)
	}
};

console.warn(person);
person.talk();

//mini act -- create function named walk and print `<name> walked 25 steps forward`

let p = {
	name: `Marvin`,
	walk: function(){
		console.log(`${p.name} walked 25 steps forward`);
	}
};

console.log(p);
p.walk();


//corpo world scenarios

let char = {
	name: `Groudon`,
	level: 1,
	hp: 100,
	atk: 50,
	tackle: function(){
		console.log(`${this.name} attacked`);
		console.log(`targetPokemon's health is now reduced to targetPokemonHealth`);
	},
	faint: function(){
		console.warn(`Pokemon fainted`);
	}
};

console.warn(char);

function Char(name, level, hp, atk){
	this.Name = name;
	this.Level = level;
	this.Hp = hp;
	this.Atk = atk;
	
	this.Tackle = function(target){
		console.log(`${this.Name} attacked ${target}`);
	};

	this.Faint = function(){
		console.warn(`${this.Name} fainted`);
	};
}

let c = new Char(`Kyogre`, 1, 100, 50,);

console.log(c);

c.Tackle(`Groudon`);

c.Faint();